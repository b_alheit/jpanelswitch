import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

public class StartPanel extends JPanel implements ActionListener {

    private JFrame container;

    StartPanel(final JFrame container){
        super();
        this.container = container;
        setLayout(new GridLayout(0, 1));

        JTextField helloField = new JTextField("Welcome to a hangman game");
        add(helloField);



        try {
            BufferedImage myPicture = ImageIO.read(new File("/home/cerecam/Benjamin_Alheit/Projects/CSC1016/JPanelSwitch/src/hangmandrawings/state11.GIF"));
            JLabel picLabel = new JLabel(new ImageIcon(myPicture));

//            picLabel.setSize(myPicture.getWidth(), myPicture.getHeight());
            add(picLabel);
//            this.getComponent(1).setSize(myPicture.getWidth(), myPicture.getHeight());
//            this.getComponent(1).setBackground(myPicture);
            this.getComponent(1).repaint();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        JTextField instruction = new JTextField("Please select your difficulty level");
        add(instruction);

        JButton easy = new JButton("Easy");
        easy.addActionListener(this);
        add(easy);

        JButton medium = new JButton("Medium");
        medium.addActionListener(this);
        add(medium);

        JButton hard = new JButton("Hard");
        hard.addActionListener(this);
        add(hard);
    }

    public void actionPerformed(ActionEvent e){
        String button = e.getActionCommand();

        if (button.equals("Easy") || button.equals("Medium") || button.equals("Hard") )
            changePanel();
    }

    private void changePanel(){
        container.getContentPane().removeAll();
        container.getContentPane().add(new GamePanel(container), BorderLayout.CENTER);
        container.revalidate();
    }

}
