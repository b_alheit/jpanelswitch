import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GamePanel extends JPanel implements ActionListener {

    private JFrame container;

    GamePanel(final JFrame container){
        super();
        this.container = container;
        setLayout(new GridLayout(2, 1));

        JTextField helloField = new JTextField("Second panel");
        add(helloField);

        JButton changePanel = new JButton("change back");
        changePanel.addActionListener(this);
        add(changePanel);
    }

    public void actionPerformed(ActionEvent e){
        String button = e.getActionCommand();

        if (button.equals("change back"))
            changePanel();
    }

    public void changePanel(){
        container.getContentPane().removeAll();
        container.getContentPane().add(new StartPanel(container), BorderLayout.CENTER);
        container.revalidate();
    }

}
