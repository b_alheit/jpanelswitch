import javax.swing.*;
import java.awt.*;

public class Test extends JFrame {

    public static final int WIDTH = 300;
    public static final int HEIGHT  = 200;

    public static void main(String[] args){
        Test gui = new Test();
        gui.setVisible(true);
    }

    public Test(){
        super();
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        add(new StartPanel(this), BorderLayout.CENTER);
    }
}
